/*
  --para compilar: gcc -o ejecutable prueba.c -pthread
  --para ejecutar: ./ejecutable
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*para crear hijos*/
#include <wait.h>
#include <sys/types.h>

#include <semaphore.h>  //para usar semaforos
#include <pthread.h>   	//para usar threads y mutex

//Definimos los semaforos como variables globales
sem_t semaforo_ma;
sem_t semaforo_ra;
sem_t semaforo_dooo;

//Definimos el mutex como variable global
pthread_mutex_t mutex;

//Funcion que imprime "ma"
void* imp_ma(void* parametro){

	sem_wait(&semaforo_ma);
	pthread_mutex_lock(&mutex);;
	/*Zona exclusiva*/
	printf("Ma");
	/*Fin zona exclusiva*/
	pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_ra);
	pthread_exit(NULL);
}

//Funcion que imprime "ra"
void* imp_ra(void* parametro){

        sem_wait(&semaforo_ra);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("ra");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_dooo);
        pthread_exit(NULL);
}

//Funcion que imprime "dooo"
void* imp_dooo(void* parametro){

        sem_wait(&semaforo_dooo);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("dooo\n");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_ma);
        pthread_exit(NULL);
}


int main(){

	int rc, value;

	//Iniciamos el mutex
	pthread_mutex_init(&mutex,NULL);

	//Iniciamos los semaforos
	sem_init(&semaforo_ma, 0, 1);
	sem_init(&semaforo_ra, 0, 0);
	sem_init(&semaforo_dooo, 0, 0);

	//Nombramos los threads
	pthread_t ma;
	pthread_t ra;
	pthread_t dooo;


	//Contador para ejecutar varias veces
	int i = 0;
	while (i < 2){

		//Creamos y lanzamos los threads
		rc = pthread_create(&ma, NULL, imp_ma, NULL);
		rc = pthread_create(&ra, NULL, imp_ra, NULL);
		rc = pthread_create(&dooo, NULL, imp_dooo, NULL);

		//Esperamos a que terminen la ejecucion
		pthread_join(ma, NULL);
		pthread_join(ra, NULL);
		pthread_join(dooo, NULL);

		//aumentamos el contador
		i = i+1;
	}

	pthread_mutex_destroy(&mutex);
	sem_destroy(&semaforo_ma);
	sem_destroy(&semaforo_ra);
	sem_destroy(&semaforo_dooo);

	pthread_exit(NULL);

	return 0;
}










