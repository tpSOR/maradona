/*
  --para compilar: gcc -o ejecutable prueba.c -pthread
  --para ejecutar: ./ejecutable
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*para crear hijos*/
#include <wait.h>
#include <sys/types.h>

#include <semaphore.h>  //para usar semaforos
#include <pthread.h>   	//para usar threads y mutex

//Definimos los semaforos como variables globales
sem_t semaforo_a;
sem_t semaforo_b;
sem_t semaforo_c;
sem_t semaforo_d;
sem_t semaforo_e;
sem_t semaforo_f;

//Definimos el mutex como variable global
pthread_mutex_t mutex;

//Funcion que imprime "ole ole ole"
void* imp_ole(void* parametro){

	sem_wait(&semaforo_a);
	pthread_mutex_lock(&mutex);;
	/*Zona exclusiva*/
	printf("Ole Ole Ole\n");
	/*Fin zona exclusiva*/
	pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_c);
	sem_post(&semaforo_d);
	pthread_exit(NULL);
}

//Funcion que imprime "ole ole ole ola"
void* imp_ola(void* parametro){

	sem_wait(&semaforo_c);
	sem_wait(&semaforo_b);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("ole ole ole ola\n\n");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_a);
        pthread_exit(NULL);
}

//Funcion que imprime "cada dia te quiero mas"
void* imp_cada(void* parametro){

        sem_wait(&semaforo_d);
	sem_wait(&semaforo_d);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("cada dia te quiero mas \n\n");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_e);
        pthread_exit(NULL);
}

void* imp_oh(void* parametro){

	sem_wait(&semaforo_e);
	pthread_mutex_lock(&mutex);
	/*Zona exclusiva*/
	printf("oooh Argentina\n");
	/*Fin zona exclusiva*/
	pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_f);
	pthread_exit(NULL);
}

void* imp_parar(void* parametro){

	sem_wait(&semaforo_f);
	pthread_mutex_lock(&mutex);
	/*Zona exclusiva*/
	printf("es un sentimiento, no puedo parar\n\n\n");
	/*Fin zona exclusiva*/
	pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_a);
	sem_post(&semaforo_b);
	pthread_exit(NULL);
}


int main(){

	int rc, value;

	//Iniciamos el mutex
	pthread_mutex_init(&mutex,NULL);

	//Nombramos los threads
	pthread_t ole_1;
	pthread_t ole_2;
	pthread_t ola;
	pthread_t cada;
	pthread_t oh;
	pthread_t parar;

	sem_init(&semaforo_a, 0, 1);
	sem_init(&semaforo_b, 0, 1);
	sem_init(&semaforo_c, 0, 0);
	sem_init(&semaforo_d, 0, 0);
	sem_init(&semaforo_e, 0, 0);
	sem_init(&semaforo_f, 0, 0);


	//Contador para ejecutar varias veces
	int i = 0;
	while (i < 1000){



		//Iniciamos los semaforos (una vez para cada ciclo---> mal!!!)

		
		rc = pthread_create(&ole_1, NULL, imp_ole, NULL);
		rc = pthread_create(&ola, NULL, imp_ola, NULL);
		rc = pthread_create(&parar, NULL, imp_parar, NULL);
		rc = pthread_create(&ole_2, NULL, imp_ole, NULL);
		rc = pthread_create(&cada, NULL, imp_cada, NULL);
		rc = pthread_create(&oh, NULL, imp_oh, NULL);

		//Esperamos a que terminen la ejecucion
		pthread_join(ole_1, NULL);
		pthread_join(ola, NULL);
		pthread_join(cada, NULL);
		pthread_join(oh, NULL);
		pthread_join(parar, NULL);
		pthread_join(ole_2, NULL);

		//aumentamos el contador
		i = i+1;
	}

	pthread_mutex_destroy(&mutex);
	sem_destroy(&semaforo_a);
	sem_destroy(&semaforo_b);
	sem_destroy(&semaforo_c);
	sem_destroy(&semaforo_d);
	sem_destroy(&semaforo_e);
	sem_destroy(&semaforo_f);

	pthread_exit(NULL);

	return 0;
}

