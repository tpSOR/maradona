/*
  --para compilar: gcc -o ejecutable prueba.c -pthread
  --para ejecutar: ./ejecutable
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*para crear hijos*/
#include <wait.h>
#include <sys/types.h>

#include <semaphore.h>  //para usar semaforos
#include <pthread.h>   	//para usar threads y mutex

sem_t semaforo_ho;
sem_t semaforo_lan;
sem_t semaforo_da;

pthread_mutex_t mutex;

void* imp_ho(void* parametro){

	sem_wait(&semaforo_ho);
	pthread_mutex_lock(&mutex);;
	/*Zona exclusiva*/
	printf("Ho");
	/*Fin zona exclusiva*/
	pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_lan);
	pthread_exit(NULL);
}

void* imp_lan(void* parametro){

        sem_wait(&semaforo_lan);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("lan");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
	sem_post(&semaforo_da);
        pthread_exit(NULL);
}

void* imp_da(void* parametro){

        sem_wait(&semaforo_da);
        pthread_mutex_lock(&mutex);
        /*Zona exclusiva*/
        printf("da");
        /*Fin zona exclusiva*/
        pthread_mutex_unlock(&mutex);
        pthread_exit(NULL);
}

int main(){

	int rc, value;

	pthread_mutex_init(&mutex,NULL);
	sem_init(&semaforo_ho, 0, 1);
	sem_init(&semaforo_lan, 0, 0);
	sem_init(&semaforo_da, 0, 0);

	pthread_t ho;
	pthread_t lan;
	pthread_t da;

	//sem_getvalue(&semaforo_ho, &value);
	//printf("%d\n", value);

	rc = pthread_create(&ho, NULL, imp_ho, NULL);
	rc = pthread_create(&lan, NULL, imp_lan, NULL);
	rc = pthread_create(&da, NULL, imp_da, NULL);


	//rc = pthread_join(ho, NULL);
	//rc = pthread_join(lan, NULL);
	//rc = pthread_join(da, NULL);


	pthread_mutex_destroy(&mutex);
	sem_destroy(&semaforo_ho);
	sem_destroy(&semaforo_lan);
	sem_destroy(&semaforo_da);

	pthread_exit(NULL);

	return 0;
}










